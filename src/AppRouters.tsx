import React from "react";
import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import PhotosPage from "./pages/PhotosPage/PhotosPage";
import TodoListPage from "./pages/TodoListPage/TodoListPage";
import TodoPage from "./pages/TodoPage/TodoPage";
import Layout from "./components/Layout/Layout";

const AppRouters = () => {
    return (
        <>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route index element={<HomePage/>}/>
                    <Route path='photos' element={<PhotosPage/>}/>
                    <Route path='todos' element={<TodoListPage/>}/>
                    <Route path='todos'>
                        <Route path=':id' element={<TodoPage/>}/>
                    </Route>
                </Route>
            </Routes>
        </>
    )
}
export default AppRouters