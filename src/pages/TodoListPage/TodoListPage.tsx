import React, { useState } from "react";
import "./TodoPage.scss"
import CreateTodosModal from "../../components/Modal/CreateTodoModal";
import { Button } from "@mui/joy";
import ToDoList from "../../components/TodoList/ToDoList";

const TodoListPage = () => {
    const [open, setOpen] = useState(false)
    const handleOpenModal = () => setOpen(true)
    const handleCloseModal = () => setOpen(false)
    return (
        <main>
            <section className="container">
                <h3 className="main__title">Task List</h3>
                <div className="create__todos__button">
                    <Button onClick={handleOpenModal}>Create new task</Button>
                </div>
                <CreateTodosModal open={open} close={handleCloseModal}/>
                <ToDoList/>
            </section>

        </main>
    )
}
export default TodoListPage