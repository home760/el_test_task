import SingleTodo from "../../components/Todo/SingleTodo"

const TodoPage = () => {
    return(
        <section>
            <SingleTodo/>
        </section>
    )
}
export default TodoPage