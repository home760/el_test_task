import React from "react";
import PhotosList from "../../components/Photos/PhotosList";
import "./PhotoPage.scss"
import SearchPhotos from "../../components/Photos/components/SearchPhotos/SearchPhotos";

const PhotosPage = () => {
    return (
        <main>
            <section className="container">
                <h3 className="main__title">Photos List</h3>
                <SearchPhotos/>
                <PhotosList/>

            </section>
        </main>
    )
}
export default PhotosPage