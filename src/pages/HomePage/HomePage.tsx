import React from "react";
import './HomePage.scss'
import ToPhotoButton from "../../components/Button/HomeButtons/ToPhotoButton";
import ToTodoListButton from "../../components/Button/HomeButtons/ToTodoListButton";

const HomePage = () => {
    return (
        <main>
            <section className="container home__wrapper">
                <div className="buttons__wrapper">
                    <ToPhotoButton/>
                    <ToTodoListButton/>
                </div>
            </section>
        </main>
    )
}
export default HomePage