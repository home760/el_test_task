import { AppState, Photo, Todo } from './types';

// TodoList Slice
export const selectorIsModal = (state: AppState): boolean => state.todoList.isModal;
export const selectorTodosList = (state: AppState): Todo[] => state.todoList.todoList;

// Photos Slice
export const selectorPhotos = (state: AppState): Photo[] => state.photos.photos;

// Helpers Slice
export const selectorIsAnimations = (state: AppState): boolean => state.helpers.isAnimation;