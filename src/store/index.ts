import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import todoListReducer from './todoListSlice';
import photosReducer from './photosSlice';
import helpersReducer from './helpersSlice';

const store = configureStore({
  reducer: {
    todoList: todoListReducer,
    photos: photosReducer,
    helpers: helpersReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export default store;