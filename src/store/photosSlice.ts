import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { actionIsAnimation } from "./helpersSlice";
import sendRequest from "../halpers/sendRequest";
import { API } from "../halpers/API";

interface Photo {
    url: string,
    title: string,
    id: number,
    thumbnailUrl: string
}

interface PhotosState {
    photos: Photo[];
}

const initialState: PhotosState = {
    photos: [],
};

const photosSlice = createSlice({
    name: "photos",
    initialState,
    reducers: {
        actionPhotosList: (state, action: PayloadAction<Photo[]>) => {
            state.photos = action.payload;
        },
    },
});


export const { actionPhotosList } = photosSlice.actions;

export const actionGetPhotos = (id: string) => async (dispatch: any) => {
    try {
        dispatch(actionIsAnimation(true));
        const response = await sendRequest(`${API}${id}`, "GET") as Photo[]; 
        if (response) {
            dispatch(actionPhotosList(response));
            return response;
        } else {
            throw new Error("Ooooops");
        }
    } catch (error) {
        console.error("Error", error);
    } finally {
        dispatch(actionIsAnimation(false));
    }
};

export default photosSlice.reducer;
