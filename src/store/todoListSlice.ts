import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Todo, TodoListState } from './types';

const todoListSlice = createSlice({
    name: 'todoList',
    initialState: {
        todoList: JSON.parse(localStorage.getItem('todoList') ?? '[]') as Todo[],
    } as TodoListState,
    reducers: {
        actionAddItemInTodoList: (state, action: PayloadAction<string>) => {
            const now = Date.now();
            const newTodo: Todo = { task: action.payload, id: now, status: false };
            state.todoList = [...state.todoList, newTodo];
            localStorage.setItem('todoList', JSON.stringify(state.todoList));
        },
        actionChangeItemInTodoList: (state, action: PayloadAction<{ id: number; change: Partial<Todo> }>) => {
            const { id, change } = action.payload;
            state.todoList = state.todoList.map(todo =>
                todo.id === id ? { ...todo, ...change } : todo
            );
            localStorage.setItem('todoList', JSON.stringify(state.todoList));
        },
        actionDeleteTodo: (state, action: PayloadAction<number>) => {
            state.todoList = state.todoList.filter(todo => todo.id !== action.payload);
            localStorage.setItem('todoList', JSON.stringify(state.todoList));
        }
    }
});

export const {
    actionAddItemInTodoList,
    actionChangeItemInTodoList,
    actionDeleteTodo
} = todoListSlice.actions;

export default todoListSlice.reducer;
