
// TodoList slice state
export interface Todo {
    id: number;
    status: boolean;
    task: string;
}

export interface TodoListState {
    isModal: boolean;
    todoList: Todo[];
}

// Photos slice state
export interface Photo {
    url: string,
    title: string,
    id: number,
    thumbnailUrl: string
}

export interface PhotosState {
    photos: Photo[];
}

// Helpers slice state
export interface HelpersState {
    isAnimation: boolean;
}

// Define the root state type
export interface AppState {
    todoList: TodoListState;
    photos: PhotosState;
    helpers: HelpersState;
}