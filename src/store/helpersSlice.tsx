import { createSlice } from "@reduxjs/toolkit";

const helpersSlice = createSlice({
    name: "helpers",
    initialState: {
        isAnimation: false,
    },
    reducers: {
        actionIsAnimation: (state, {payload}) => {
            state.isAnimation = payload
        },

    }
})
export const {
    actionIsAnimation
} = helpersSlice.actions


export default helpersSlice.reducer