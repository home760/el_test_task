import { Button } from "@mui/joy"
import ChangeTodosModal from "../Modal/ChangeTodoModal"
import { useState } from "react"
import React, {FC} from 'react'

interface ChangeButtonProps {
    id: number;
    task: string;
    variant?: "text" | "outlined" | "contained"; 
}

const ChangeButton: FC<ChangeButtonProps> = ({id, task, variant}) => {
    const [open, setOpen] = useState(false)
    const handleCOpenChangeModal = () => setOpen(true)
    const close = () => setOpen(false)
    return (
        <>
            <Button  open={open} variant={variant} size="md" onClick={handleCOpenChangeModal}>Change</Button>
            <ChangeTodosModal open={open} close={close} id={id} task={task}/>
        </>
    )
}


export default ChangeButton