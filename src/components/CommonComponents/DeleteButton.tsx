import { Button } from "@mui/joy"
import { useDispatch } from "react-redux"
import { actionDeleteTodo } from "../../store/todoListSlice"
import React, {FC} from "react"

interface DeleteButtonProps {
    id: number,
    status?: boolean
}


const DeleteButton: FC<DeleteButtonProps> = ({id}) => {
    const dispatch = useDispatch()
    const handleDeleteTodo = () => dispatch(actionDeleteTodo(id))
    return (
        <Button onClick={handleDeleteTodo}>Delete</Button>
    )
}

export default DeleteButton