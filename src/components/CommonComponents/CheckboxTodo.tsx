
import React, {FC} from 'react';
import Checkbox from '@mui/joy/Checkbox';
import { useDispatch } from 'react-redux';
import { actionChangeItemInTodoList } from '../../store/todoListSlice';

interface CheckboxTodoProps {
    id: number,
    status: boolean
}

const CheckboxTodo: FC<CheckboxTodoProps> = ({id, status}) => {
    const dispatch = useDispatch()
    const handleToggleCheckbox = (event: React.ChangeEvent<HTMLInputElement>) => dispatch(actionChangeItemInTodoList({id:id, change:{status: event.target.checked}}));
    return (
            <Checkbox checked={status} onChange={handleToggleCheckbox}/>
    )
}

export default CheckboxTodo