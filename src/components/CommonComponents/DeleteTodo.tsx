import React, {FC} from 'react'
import { useState } from 'react'
import DeleteTodoModal from '../Modal/DeleteTodoModal'

interface DeleteTodoProps {
    id: number,
    children: any,
    task: string
}

const DeleteTodo: FC<DeleteTodoProps> = ({id, children, task}) => {
    const [open, setOpen] = useState(false)
    const handleOpenModal = () => setOpen(true)
    const handleClose = () => setOpen(false)
    return (
        <>
            <div id={id} className='cross__wrapper' onClick={handleOpenModal}>
                {children}
            </div>
            <DeleteTodoModal open={open} handleClose={handleClose} id={id} task={task}/>
        </>
    )
}

export default DeleteTodo