import React, {FC} from "react"

interface PhotosItemProps {
    url: string,
    title: string
}
const PhotosItem: FC<PhotosItemProps> = ({url, title}) => {
    return (
        <>
            <img className="img" style={{ minHeight: '150px' }}  src={url} alt={title} />
            <p className="title">{title}</p>
        </>
    )
}

export default PhotosItem