import { Button, FormControl, FormHelperText, Input, Stack } from "@mui/joy"
import Search from "./svg/search.svg?react"
import React, { useState } from "react"
import { useDispatch } from "react-redux"
import { actionGetPhotos } from "../../../../store/photosSlice"

const SearchPhotos = () => {
    const dispatch = useDispatch()
    const [data, setData] = useState('')
    const [prevId, setPrevId] = useState('')
    const [disabled, setDisabled] = useState(false)
    const num = Number(data)
    const noError = data === "" || (num >=1 && num <=100 && typeof(num) === "number")

    const handleInputdata = (event: React.ChangeEvent<HTMLInputElement>) => {
        setData(event.target.value)
    }
    
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        if (noError && data !== "") {
            dispatch(actionGetPhotos(data))
            setPrevId(data)
            setData(''); 
        }
    }
    const handleDisable = (event:  React.ChangeEvent<HTMLInputElement>) => {
        setDisabled(event.target.value == prevId ? true : false)
    }


    return (
        <div className="search__wrapper">
            <form 
            onSubmit={handleSubmit}
            >
                <Stack spacing={2}>
                    <FormControl error>
                    <Input 
                        startDecorator={<Search/>}
                        endDecorator={<Button disabled={disabled} type="submit">Search</Button>}
                        fullWidth
                        variant="outlined"
                        placeholder="Enter number from 1 to 100"
                        onChange={(event) => {
                            handleInputdata(event)
                            handleDisable(event)
                        }}
                        error={!noError}
                        value={data}
                    />
                    {!noError && 
                        <FormHelperText className="error__text">
                        Enter number from 1 to 100.
                        </FormHelperText>
                    }
                    </FormControl>
                </Stack>
                    
            </form>

            {prevId && <h3 className="main__title">No {prevId}</h3>}
        </div>
    )
}
export default SearchPhotos