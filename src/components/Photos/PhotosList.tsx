import { useSelector } from "react-redux"
import PhotosItem from "./components/PhotosItem"
import "./PhotosList.scss"
import { selectorPhotos } from "../../store/selectors"

type Photo = {
    url: string,
    title: string,
    id: number,
    thumbnailUrl: string
}

const PhotosList = () => {
    const photos = useSelector(selectorPhotos)
    console.log(photos);
    
    return (
        <div className="photoslist__wrapper" >
            {photos.map((photo: Photo) => (
                <div className="photo__wrapper" key={photo.id}>
                    <PhotosItem url={photo.thumbnailUrl} title={photo.title}/>
                </div>
            ))}
        </div>
    )
}
export default PhotosList