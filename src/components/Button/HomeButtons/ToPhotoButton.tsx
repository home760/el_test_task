import React from "react";
import Button from '@mui/joy/Button';
import { useNavigate } from "react-router-dom";

const ToPhotoButton = () => {
    const navigate = useNavigate()
    const handleToPhotos = () => navigate('/photos')
    return (
        <Button color="primary" size="lg" onClick={handleToPhotos}>Go to Photos</Button>
    )
}
export default ToPhotoButton