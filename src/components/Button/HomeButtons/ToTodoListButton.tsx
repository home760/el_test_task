import React from "react";
import Button from '@mui/joy/Button';
import { useNavigate } from "react-router-dom";

const ToTodoListButton = () => {
    const navigate = useNavigate()
    const handleToTodoList = () => navigate('/todos')
    return (
            <Button color="primary" size="lg" onClick={handleToTodoList}>Create new task</Button>
    )
}
export default ToTodoListButton