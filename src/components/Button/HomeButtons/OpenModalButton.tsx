import Button from "@mui/joy/Button";
import React, {FC} from "react";

interface OpenModalButtonProps {
    children: string;
    className: string
}


const OpenModalButton: FC<OpenModalButtonProps> = ({children, className}) => {

    return (
        <Button className={className} color="primary" size="lg">{children}</Button>
    )
}

export default OpenModalButton