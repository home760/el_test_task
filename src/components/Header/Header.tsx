import React from "react";
import './Header.scss'
import { Outlet, useNavigate } from "react-router-dom";

const Header = () => {
    const navigate = useNavigate()
    const handlerGoHome = () => navigate('/')
    return ( 
        <>
            <header className="header">
                <div className="header__wrapper">
                    <p className="gohome__link" onClick={handlerGoHome}>Home page</p>
                </div>
            </header>
            <Outlet/>
        </>
    )
}
export default Header