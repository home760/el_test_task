
import React, {FC} from "react";
import cx from "classnames"
import { useNavigate } from 'react-router-dom';
interface TaskProps {
    status: boolean,
    task: string,
    id: number
}

const Task: FC<TaskProps> = ({status, task, id}) => {
    const navigate = useNavigate()
    const handleTodoPage = () => navigate(`${id}`)

    return (
        <p onClick={handleTodoPage} className={cx('todo__text', {"not__active":status })}>{task}</p>
    )
}


export default Task