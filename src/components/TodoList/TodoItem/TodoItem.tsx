
import React, {FC} from 'react';
import CheckboxTodo from '../../CommonComponents/CheckboxTodo';
import DeleteTodo from '../../CommonComponents/DeleteTodo';
import Cross from'./components/svg/cross.svg?react'
import ChangeButton from '../../CommonComponents/ChangeButton';
import Task from './components/Task';

interface TodoItemProps {
    todo: {
        id: number,
        status: boolean,
        task: string
    }
}
const TodoItem: FC<TodoItemProps> = ({todo}) => {
    
    return (
        <div className='todo__item__wrapper'>
            <CheckboxTodo id={todo.id} status={todo.status}/>
            <Task status={todo.status} task={todo.task} id={todo.id}/>
            <ChangeButton id={todo.id} task={todo.task} variant="outlined"/>
            <DeleteTodo id={todo.id} task={todo.task}><Cross/></DeleteTodo>
            
        </div>
    )
}
export default TodoItem