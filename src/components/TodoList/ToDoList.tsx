import React, { FC, useState } from 'react';
import './Todos.scss';
import { useSelector } from 'react-redux';
import { selectorTodosList } from '../../store/selectors';
import TodoItem from './TodoItem/TodoItem';
import { Option, Select, Stack } from '@mui/joy';

interface Todo {
    id: number;
    status: boolean;
    task: string;
}

type Todos = Todo[];

const ToDoList: FC = () => {
    const [sortValue, setSortValue] = useState<string>('');
    const todos = useSelector(selectorTodosList);
    const sortedTodos = todos && [...todos].sort((a, b) => Number(a.status) - Number(b.status));

    const handleFilter = (_event: React.MouseEvent | React.KeyboardEvent | React.FocusEvent | null, value: string | null) => {
        setSortValue(value || '');
    };

    const newTodos = sortedTodos.filter((todo: Todo) => {
        if (sortValue === '') return true;
        return todo.status === (sortValue === 'true');
    });

    return (
        <section className='todos__wrapper'>
            <div className='select__todo'>
                <Stack spacing={2} alignItems="flex-end">
                    <Select
                        placeholder="Choose one..."
                        onChange={handleFilter} 
                        name="foo"
                        sx={{ minWidth: 200 }}
                    >
                        <Option value="">All</Option>
                        <Option value="false">To do</Option>
                        <Option value='true'>Done</Option>
                    </Select>
                </Stack>
            </div>
            {newTodos.length === 0 && <p>No todos found.</p>}
            {newTodos && newTodos.map((todo: Todo) => (
                <div key={todo.id} className="todos__list">
                    <TodoItem todo={todo} />
                </div>
            ))}
        </section>
    );
};

export default ToDoList;
