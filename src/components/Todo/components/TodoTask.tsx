
import React,{FC} from "react"
import cx from "classnames"

interface TodoTaskProps {
    task: string,
    status: boolean
}

const TodoTask: FC<TodoTaskProps> = ({task, status}) => {
    return (
        <div>
            <p className={cx('todo__text', {"not__active":status })}>{task}</p>
        </div>

    )
}

export default TodoTask