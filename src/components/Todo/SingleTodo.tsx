import "./SingleTodo.scss"
import { useParams } from "react-router-dom"
import TodoTask from "./components/TodoTask";
import ChangeButton from "../CommonComponents/ChangeButton";
import { useSelector } from "react-redux";
import { selectorTodosList } from "../../store/selectors";
import CheckboxTodo from "../CommonComponents/CheckboxTodo";
import DeleteTodo from "../CommonComponents/DeleteTodo";
import { Button } from "@mui/joy";

type todo = {
    id: number,
    status: boolean,
    task: string
}

const SingleTodo = () => {
    const {id} = useParams()
    const todo = useSelector(selectorTodosList).find((item: todo) => item.id === Number(id))
    return (
        <main className="container">
            <section className="section">
                <h3 className="title">Task</h3>
                <div className="task">
                    <TodoTask task={todo.task} status={todo.status}/>
                    <CheckboxTodo id={todo.id} status={todo.status}/>
                </div>
                <div className="buttons__wrapper">
                    <ChangeButton id={todo.id} task={todo.task}/>
                    <DeleteTodo task={todo.task} id={todo.id} >
                        <Button>Delete</Button>
                    </DeleteTodo>
                </div>
            </section>
        </main>
    )
}
export default SingleTodo