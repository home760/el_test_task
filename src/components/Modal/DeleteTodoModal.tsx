import React, {FC} from "react";
import { Button, Modal, ModalClose, Sheet, Typography } from "@mui/joy"
import "./DeleteTodoModal.scss"
import { useNavigate } from "react-router-dom";
import DeleteButton from "../CommonComponents/DeleteButton";

interface DeleteTodoModalProps {
    open: boolean, 
    handleClose: () => void, 
    task: string,
    id: number
}

const DeleteTodoModal: FC<DeleteTodoModalProps> = ({open, handleClose, task, id}) => {
    const navigate = useNavigate()
    const handleGoBack = () => navigate('/todos')
    return (
        <Modal
        aria-labelledby="modal-title"
        aria-describedby="modal-desc"
        open={open}
        onClose={handleClose}
        sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
        >
            <Sheet
            variant="outlined"
            sx={{
                maxWidth: 500,
                borderRadius: 'md',
                p: 3,
                boxShadow: 'lg',
            }}
            >
            <ModalClose/>
            <Typography
                className="modal__content"
                component="h2"
                id="modal-title"
                level="h4"
                textColor="inherit"
                fontWeight="lg"
                mb={1}
            >
                Are you sure you want to delete the task?
            </Typography>
            <Typography id="modal-desc" textColor="text.tertiary">
                {task}
            </Typography>
            <div className="delete__modal__buttons">
                <div onClick={handleGoBack}>
                    <DeleteButton id={id}/>
                </div>
                <Button onClick={handleClose}>No</Button>
            </div>
            </Sheet>
        </Modal>
    )
}

export default DeleteTodoModal