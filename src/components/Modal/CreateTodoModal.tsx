import React, {FC} from 'react';
import Modal from '@mui/joy/Modal';
import ModalDialog from '@mui/joy/ModalDialog';
import ModalClose from '@mui/joy/ModalClose';
import DialogContent from '@mui/joy/DialogContent';
import Stack from '@mui/joy/Stack';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Textarea from '@mui/joy/Textarea';
import { Button, Typography } from '@mui/joy';
import { useDispatch } from 'react-redux';
import { actionAddItemInTodoList } from '../../store/todoListSlice';
import { useState } from 'react';

interface CreateTodosModalProps {
    close: () => void,
    open: boolean
}

const CreateTodosModal: FC<CreateTodosModalProps> = ({open, close}) => {
    const dispatch = useDispatch()
    const [data, setData] = useState('')
    const handleAreaValue = (event: React.ChangeEvent<HTMLTextAreaElement>) => setData(event.target.value); 
    const handleAddToList = (value: string) => dispatch(actionAddItemInTodoList(value))

    return (
        <Modal open={open} onClose={close}>
            <ModalDialog>
                <ModalClose/>
                <Typography component="h2" fontWeight="lg" level="h4">Create new task</Typography>
                <DialogContent>Fill in your task here.</DialogContent>
                <form
                    onSubmit={(event) => {
                        event.preventDefault();
                        handleAddToList(data)
                        close()
                }}
                >
                     <Stack spacing={2}>
                        <FormControl>
                            <FormLabel>Task</FormLabel>
                            <Textarea minRows={3} onChange={handleAreaValue} autoFocus required/>
                        </FormControl>
                        <Button type="submit">Submit</Button>
                    </Stack>
                </form>
            </ModalDialog>
        </Modal>
    )
}

export default CreateTodosModal