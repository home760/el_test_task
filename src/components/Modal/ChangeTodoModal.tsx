import React, {FC} from 'react';
import Modal from '@mui/joy/Modal';
import ModalDialog from '@mui/joy/ModalDialog';
import ModalClose from '@mui/joy/ModalClose';
import DialogContent from '@mui/joy/DialogContent';
import Stack from '@mui/joy/Stack';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Textarea from '@mui/joy/Textarea';
import { Button, Typography } from '@mui/joy';
import { useDispatch } from 'react-redux';
import { useState } from 'react';
import { actionChangeItemInTodoList } from '../../store/todoListSlice';

interface ChangeTodosModalProps {
    open: boolean,
    close: () => void,
    id: number,
    task: string
}

const ChangeTodosModal: FC<ChangeTodosModalProps> = ({open, close, id, task}) => {
    const dispatch = useDispatch()
    const [data, setData] = useState('')
    const handleAreaValue = (event: React.ChangeEvent<HTMLTextAreaElement>) => setData(event.target.value); 
    const handleChangeTodo = () => dispatch(actionChangeItemInTodoList({id:id, change: {task: data}}))

    return (
        <Modal open={open} onClose={close}>
            <ModalDialog>
                <ModalClose/>
                <Typography component="h2" level="h4" fontWeight="lg">
                    Change task
                </Typography>
                <DialogContent>Fill in your task here.</DialogContent>
                <form
                    onSubmit={(event) => {
                        data ? handleChangeTodo() : close()
                        event.preventDefault();
                        close()
                }}
                >
                     <Stack spacing={2}>
                        <FormControl>
                            <FormLabel>Task</FormLabel>
                            <Textarea minRows={3} onChange={handleAreaValue} defaultValue={task} autoFocus required/>
                        </FormControl>
                        <Button type="submit">Submit</Button>
                    </Stack>
                </form>
            </ModalDialog>
        </Modal>
    )
}

export default ChangeTodosModal