interface RequestOptions extends RequestInit {
    headers?: Record<string, string>;
    body?: any; 
}
const sendRequest = async (
    url: string,
    method: string = "GET",
    options?: RequestOptions
): Promise<any | Response | undefined> => {
    try {
        const response = await fetch(url, { method, ...options });

        if (response.ok) {
            if (method === "DELETE") {
                return response;
            }
            return response.json();
        } else {
            throw new Error(`Request failed with status ${response.status}`);
        }
    } catch (error) {
        console.error("Request error:", error);
        throw error; 
    }
};

export default sendRequest;