import './reset.css'
import './halpers/variables.scss'
import './halpers/base.scss'
import AppRouters from './AppRouters'
import Loader from './halpers/Loader/Loader'
import { useSelector } from 'react-redux';
import { selectorIsAnimations } from './store/selectors';

function App() {
  const isAnimation = useSelector(selectorIsAnimations)
  return (
    <>
      {isAnimation && <Loader/>}
      <AppRouters/>
    </>
  )
}

export default App
